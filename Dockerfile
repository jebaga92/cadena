
### STAGE 1: Build ###

# We label our stage as ‘builder’
FROM node:10-alpine as builder

COPY package.json  ./
RUN npm i 
## Storing node modules on a separate layer will prevent unnecessary npm installs at each build

RUN  mkdir /app && mv ./node_modules ./app

WORKDIR /app

COPY . /app

RUN  npm run predeploy

### STAGE 2: Setup ###

FROM nginx:1.14.1-alpine

## Copy our default nginx config
COPY nginx/default.conf /etc/nginx/conf.d/

## Remove default nginx website
RUN rm -rf /usr/share/nginx/html/*

## From ‘builder’ stage copy over the artifacts in dist folder to default nginx public folder
COPY --from=builder /app/build /usr/share/nginx/html
COPY ./letsencrypt /etc/letsencrypt
COPY /acme-challenge  /usr/share/nginx/html/acme-challenge
CMD ["nginx", "-g", "daemon off;"]



# docker build -t us.gcr.io/everisconf/my-trust-webside:1.0.5 -f Dockerfile .
# gcloud docker -- push us.gcr.io/everisconf/my-trust-webside:1.0.5

# docker run  -itd  --name rmy-trust-webside -p 80:80  -p 443:443   us.gcr.io/everisconf/my-trust-webside:1.0.5
