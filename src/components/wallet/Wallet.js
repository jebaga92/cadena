import React, { Component, Fragment } from "react";
import { withTranslation } from "react-i18next";
import { appSection, phone, downloadBtnIcons, appSectionMobile } from '../../images';
import DownloadButton from './DownloadButton';

class Download extends Component {
  render() {
    const { t } = this.props;

    const walletContent = (
      <Fragment>
        <div className="main-container-app">
          <div
            className="background-image-app"
          >
            <p className="wallet-title-app">{t("download.title", { framework: "react-i18next" })}</p>
            <p className="wallet-description-app">{t("download.description", { framework: "react-i18next" })}</p>
            <ol className="wallet-steps-list">
              {t("download.steps", {
                returnObjects: true
              }).map(step => (
                <li>{step}</li>
              ))}
            </ol>
          </div>
        </div>
        <div className="description-container-app">
          <div className="phone-container">
            <img src={phone} alt="phone" className="phone-img" />
          </div>
          <div className="phone-btns-container">
              <p className="download-section-title">{t("download.downloadTitle", { framework: "react-i18next" })}</p>
              {t("download.downloadBtns", {
                returnObjects: true
              }).map(btn => (
                <DownloadButton
                  icon={downloadBtnIcons[btn.icon]}
                  subtitle={btn.subtitle}
                  title={btn.title}
                  link={btn.link}
                />
              ))}
          </div>
        </div>
      </Fragment>
    );

    return (
      <Fragment>
        <div
        style={{
          background: `url("${appSection}")`
        }}
        className="container-wallet container-desktop">
         {walletContent}
      </div>
      <div
        style={{
          background: `url("${appSectionMobile}")`
        }}
        className="container-wallet container-mobile">
          {walletContent}
      </div>
      </Fragment>
    );
  }
}

export default withTranslation("translations")(Download);
