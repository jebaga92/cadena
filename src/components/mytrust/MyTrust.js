import React, { Component } from "react";
import { withTranslation, Trans } from "react-i18next";
import { iconDown } from "../../images/index";
import Typed from "react-typed";
import { showroom, showroomMobile } from "../../images/index";

class MyTrust extends Component {
  state = {
    headerTop: true
  };

  render() {
    const { t } = this.props;

    const contentMyTrust = (
      <div className="content-my-trust">
        <div className="content-text-my-trust content-info-mytrust">
          <div></div>
          <div style={{ width: "90%" }} className="content-text-my-trust">
            <h1 className="t-white">
              <Trans i18nKey="myTrust.title" />
            </h1>
            <Typed
              className="messages-my-trust"
              strings={t("myTrust.benefits", { returnObjects: true })}
              typeSpeed={40}
              backSpeed={20}
              loop
            />
          </div>
        </div>
        <img
          onClick={() => this.props.scrollToSection("Benefits")}
          className="circle-icon-down"
          src={iconDown}
          alt="icon-down"
        />
      </div>)

    return (
      <div>
        <div
          style={{
            backgroundImage: `url("${showroom}")`
          }}
          className="my-trust my-trust-dektop"
        >
          {contentMyTrust}
        </div>

        <div
          style={{ backgroundImage: `url("${showroomMobile}")` }}
          className="my-trust my-trust-mobile"
        >
          {contentMyTrust}
        </div>
      </div>


    );
  }
}

export default withTranslation("translations")(MyTrust);
