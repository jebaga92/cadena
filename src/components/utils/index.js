
import { EMAIL_SERVICE, RECIPIENT_EMAIL } from './constants';

export const getTemplate = (values) => {
  return `<html xmlns='http://www.w3.org/1999/xhtml'><head><title>Conoce más acerca de MyTrust</title> <meta charset=utf-8> <style type='text/css'> .fontSizeDescription { font-size: 24px; font-family: sans-serif; } .buttonHover:hover { opacity: 0.6; } .fontSizeLinkText { font-size: 22 } .linkEveris { color: #b1afb7; font-size: 20px; text-align: center; font-family: sans-serif; } .descriptionContainer { padding-bottom: 2em; } @media screen and (max-width: 480px) { .fontSizeDescription { font-size: 14px; } .descriptionContainer { padding-right: 1em; padding-left: 1em; } .linkEveris { color: 3b1afb7; font-size: 11px; } .fontSizeLinkText { font-size: 22px } } @media screen and (max-width: 3200px) { .fontSizeDescription { font-size: 11px; } .linkEveris { color: 3b1afb7; font-size: 9px; } .fontSizeLinkText { font-size: 11px } } </style> </head> <body bgcolor='#f9fafa'> <table align='center' style='text-align:center;background-color: #fff'> <tr> <td align='center' style='text-align:center;background-color: #fff'> <div style='background-color: #fff; margin-left: 2em; margin-right: 2em'> <div> <img style='width: 60px;height: 60px;margin-bottom: 2em' src='https://user-images.githubusercontent.com/32302890/62794333-7065e780-ba99-11e9-95ee-db20e5db52ff.png' alt='everis-logo' /> <h1 style='color:#8f8d96;font-size:2.5em;font-family:sans-serif; text-align:left'>My <span style='color: #251051'>Trust</span></h1> </div> <div class='descriptionContainer'><p class='fontSizeDescription' style='color: #8f8d96; text-align: left'><span style='font-weight: bold; color: black;'>Nombre: </span>${values.name}</p><p class='fontSizeDescription' style='color: #8f8d96; text-align: left'><span style='font-weight: bold; color: black;'>Compañía: </span>${values.company}</p><p class='fontSizeDescription' style='color: #8f8d96; text-align: left'><span style='font-weight: bold; color: black;'>Correo electrónico: </span>${values.email}</p><p class='fontSizeDescription' style='color: #8f8d96; text-align: left'><span style='font-weight: bold; color: black;'>Número telefónico: </span>${values.phone}</p> <p class='fontSizeDescription' style='color: #8f8d96; text-align: left'><span style='font-weight: bold; color: black;'>Mensaje: </span>${values.message}</p></div> </div> <div style='background-color: #251051; padding-top: 1em; padding-bottom: 1em'> <img style='width: 80px;height: 40px;margin-bottom: 2em' src='https://user-images.githubusercontent.com/32302890/62796364-aeb1d580-ba9e-11e9-9198-a6d93cacb306.png' alt='everis-logo' /> <p style='color: #fff;font-size: 14px;font-weight: bold;margin:0;font-family:sans-serif'>Powered by everis</p> <p style='color: #fff;font-size: 10px;margin:0;font-family:sans-serif'>Todos los derechos reservados.</p> </div> </td> </tr> </table></body> </html>`
};

export const sendEmail = async (values, subject) => {
  try {
    const sendInformation = await fetch(EMAIL_SERVICE, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: RECIPIENT_EMAIL,
        subject: subject,
        html: getTemplate(values)
      })
    })
    return sendInformation;
  } catch (error) {
    throw error;
  }
}

export const getObjectKey = (object, value) => {
  return Object.keys(object).find(key => object[key] === value)
};